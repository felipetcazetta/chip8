#include "controllers.h"

#include <SDL.h>

uint8_t g_controllerConfig[NUMBER_OF_KEYS][BINDINGS_PER_KEY] = {
        { SDL_SCANCODE_X, SDL_SCANCODE_PAGEDOWN }, // 0x0
        { SDL_SCANCODE_1, SDL_SCANCODE_KP_1 }, // 0x1
        { SDL_SCANCODE_2, SDL_SCANCODE_KP_2 }, // 0x2
        { SDL_SCANCODE_3, SDL_SCANCODE_KP_3 }, // 0x3
        { SDL_SCANCODE_Q, SDL_SCANCODE_KP_4 }, // 0x4
        { SDL_SCANCODE_W, SDL_SCANCODE_KP_5 }, // 0x5
        { SDL_SCANCODE_E, SDL_SCANCODE_KP_6 }, // 0x6
        { SDL_SCANCODE_A, SDL_SCANCODE_KP_7 }, // 0x7
        { SDL_SCANCODE_S, SDL_SCANCODE_KP_8 }, // 0x8
        { SDL_SCANCODE_D, SDL_SCANCODE_KP_9 }, // 0x9
        { SDL_SCANCODE_Z, SDL_SCANCODE_PAGEUP }, // 0xA
        { SDL_SCANCODE_C, SDL_SCANCODE_HOME }, // 0xB
        { SDL_SCANCODE_4, SDL_SCANCODE_END }, // 0xC
        { SDL_SCANCODE_R, SDL_SCANCODE_KP_MINUS }, // 0xD
        { SDL_SCANCODE_F, SDL_SCANCODE_KP_PLUS }, // 0xE
        { SDL_SCANCODE_V, SDL_SCANCODE_KP_PERIOD } // 0xF
};



bool isPressed(uint8_t key)
{
    const Uint8* keyboardState = SDL_GetKeyboardState(NULL);
    uint8_t i;
    for (i=0; i < BINDINGS_PER_KEY; i++)
    {
        if( keyboardState[ g_controllerConfig[key][i] ] )
            return true;
    }

    return false;
}



uint8_t awaitKey()
{
    while (true)
    {
        const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
        uint8_t i, j;
        for (i = 0; i < NUMBER_OF_KEYS; i++)
        {
            for (j = 0; j < BINDINGS_PER_KEY; j++)
            {
                if ( keyboardState[ g_controllerConfig[i][j] ] )
                    return i;
            }
        }
    }
}