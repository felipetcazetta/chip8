#ifndef CHIP8_AUDIO_H
#define CHIP8_AUDIO_H

#define BEEP_AMPLITUDE 127
#define BEEP_FREQUENCY_HZ 440
#define SAMPLE_RATE_HZ 48000
#define SOUND_BUFFER_SIZE 1024

#define WAVEFORM_SINE 0
#define WAVEFORM_TRIANGLE 1
#define WAVEFORM_SAWTOOTH 2
#define WAVEFORM_PULSE 3
#define NUM_WAVEFORMS 4

// Só usado com WAVEFORM_PULSE selecionado
#define PULSE_DUTY_CYCLE 0.5

void audioStart();
void beepStart();
void beepStop();
void audioClose();

#endif //CHIP8_AUDIO_H
