#include <SDL.h>
#include "graphics.h"
#include "processor.h"
#include "audio.h"

int main(int argC, char* argV[])
{
    if(argC != 2)
        return 0;

    graphicsStart();
    loadRom(argV[1]);

    audioStart();

    SDL_Thread* processorThread = SDL_CreateThread(processorLoop, "processor", NULL);
    SDL_DetachThread(processorThread);

    graphicsLoop(NULL);

    audioClose();
    graphicsClose();
    return 0;
}
