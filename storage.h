#ifndef CHIP8_STORAGE_H
#define CHIP8_STORAGE_H

#include <stdint.h>

#define MEMORY_SIZE 0x1000
#define NUM_REGISTERS 0x10
#define STACK_SIZE 0x18

#define MEMORY_STARTING_ADDRESS 0x200

typedef struct
{
    uint8_t memory[MEMORY_SIZE];

    uint8_t registers[NUM_REGISTERS];
    uint16_t addressRegister;
    uint16_t stack[STACK_SIZE];

    uint8_t delayTimer;
    uint8_t soundTimer;

    uint16_t programCounter;
    uint8_t stackPointer;
} Storage;

#endif //CHIP8_STORAGE_H
