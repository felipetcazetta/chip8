#include "audio.h"

#include <SDL.h>
#include <math.h>
#include <string.h>

Sint8* preBuffers[NUM_WAVEFORMS];
int preBufferSize = 0;

int beepWaveform = WAVEFORM_PULSE;

SDL_AudioSpec want, have;
SDL_AudioDeviceID dev;



int min(int a, int b)
{
    return a < b ? a : b;
}



void sdlAudioCallback(void* userdata, Uint8* buffer, int bytesToCopy)
{
    int* preBufferIndex = (int*) userdata;
    int copiedBytes = 0;

    while (copiedBytes < bytesToCopy)
    {
        int batchSize = min(bytesToCopy - copiedBytes, preBufferSize - *preBufferIndex);
        memcpy(buffer + copiedBytes, preBuffers[beepWaveform] + *preBufferIndex, batchSize);

        copiedBytes += batchSize;
        *preBufferIndex = (*preBufferIndex + batchSize) % preBufferSize;
    }
}



void preGenerateBuffers()
{
    int i;
    double time;
    double x = 0;
    for (i=0; x < 1; i++)
    {
        time = i / (double) SAMPLE_RATE_HZ;
        x = time * BEEP_FREQUENCY_HZ;
    }

    preBufferSize = i;

    for (i=0; i < NUM_WAVEFORMS; i++)
        preBuffers[i] = malloc(preBufferSize * sizeof(Sint8) );

    for (i=0; i < preBufferSize; i++)
    {
        time = i / (double) SAMPLE_RATE_HZ;
        x = time * BEEP_FREQUENCY_HZ;

        preBuffers[WAVEFORM_SINE][i] = BEEP_AMPLITUDE * sin(2*M_PI * x);
        preBuffers[WAVEFORM_TRIANGLE][i] = BEEP_AMPLITUDE * ((x < 0.5) ? (4*x - 1) : (-4*x + 3));
        preBuffers[WAVEFORM_SAWTOOTH][i] = BEEP_AMPLITUDE * (2*x - 1);
        preBuffers[WAVEFORM_PULSE][i] = BEEP_AMPLITUDE * (x < PULSE_DUTY_CYCLE ? 1 : -1);
    }
}



void audioStart()
{
    int preBufferIndex = 0;
    preGenerateBuffers();

    SDL_memset(&want, 0, sizeof(want));
    want.freq = SAMPLE_RATE_HZ;
    want.format = AUDIO_S8;
    want.channels = 1;
    want.samples = SOUND_BUFFER_SIZE;
    want.callback = sdlAudioCallback;
    want.userdata = &preBufferIndex;

    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);
}



void beepStart()
{
    SDL_PauseAudioDevice(dev, 0);
}



void beepStop()
{
    SDL_PauseAudioDevice(dev, 1);
}



void audioClose()
{
    int i;
    for (i=0; i < NUM_WAVEFORMS; i++)
        free(preBuffers[i]);

    SDL_CloseAudioDevice(dev);
}