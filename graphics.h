#ifndef CHIP8_GRAPHICS_H
#define CHIP8_GRAPHICS_H

#define PIXEL_MULTIPLIER 10
#define WIDTH 64
#define HEIGHT 32

#include <stdint.h>

#define FRAMES_PER_SECOND 30

void graphicsStart();
void graphicsClose();

int graphicsLoop(void* dummy); // Recebe void* e retorna int por compatibilidade com threads
void clearScreen();
uint8_t draw(uint8_t baseX, uint8_t baseY, uint8_t height, const uint8_t* buffer);

#endif //CHIP8_GRAPHICS_H
