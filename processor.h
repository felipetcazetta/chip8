#ifndef CHIP8_PROCESSOR_H
#define CHIP8_PROCESSOR_H

#include <stdint.h>

#define CLOCK_SPEED_HZ 500
#define TIMER_COUNTDOWNS_PER_SECOND 60

void loadRom(const char* filename);
int processorLoop(void*); // Recebe void* e retorna int por compatibilidade com threads

#endif //CHIP8_PROCESSOR_H
