#include "processor.h"

#include <SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include "audio.h"
#include "controllers.h"
#include "graphics.h"
#include "storage.h"

#define REAL_CLOCK_CYCLES_PER_INSTRUCTION (CLOCKS_PER_SEC / CLOCK_SPEED_HZ)

/*
    NNN: address
    NN: 8-bit constant
    N: 4-bit constant
    X and Y: 4-bit register identifier
    PC : Program Counter
    I : 16bit register (For memory address) (Similar to void pointer)
    VN: One of the 16 available variables. N may be 0 to F (hexadecimal)
*/



// Inicializa somente as fontes nos primeiros endereços de memória, cada linha é um caracter hexadecimal
Storage storage = {{
                             0xF0, 0x90, 0x90, 0x90, 0xF0,
                             0x20, 0x60, 0x20, 0x20, 0x70,
                             0xF0, 0x10, 0xF0, 0x80, 0xF0,
                             0xF0, 0x10, 0xF0, 0x10, 0xF0,
                             0x90, 0x90, 0xF0, 0x10, 0x10,
                             0xF0, 0x80, 0xF0, 0x10, 0xF0,
                             0xF0, 0x80, 0xF0, 0x90, 0xF0,
                             0xF0, 0x10, 0x20, 0x40, 0x40,
                             0xF0, 0x90, 0xF0, 0x90, 0xF0,
                             0xF0, 0x90, 0xF0, 0x10, 0xF0,
                             0xF0, 0x90, 0xF0, 0x90, 0x90,
                             0xE0, 0x90, 0xE0, 0x90, 0xE0,
                             0xF0, 0x80, 0x80, 0x80, 0xF0,
                             0xE0, 0x90, 0x90, 0x90, 0xE0,
                             0xF0, 0x80, 0xF0, 0x80, 0xF0,
                             0xF0, 0x80, 0xF0, 0x80, 0x80
                     }};



void loadRom(const char* filename)
{
    FILE* rom = fopen(filename, "r");
    fread(&storage.memory[MEMORY_STARTING_ADDRESS], 1, MEMORY_SIZE - MEMORY_STARTING_ADDRESS, rom);
    fclose(rom);

    storage.programCounter = MEMORY_STARTING_ADDRESS;
    storage.stackPointer = 0;
}



void countdownTimers()
{
    if(storage.delayTimer != 0)
        storage.delayTimer--;

    if(storage.soundTimer != 0)
    {
        beepStart();
        storage.soundTimer--;
    }
    else
        beepStop();
}



uint16_t getNs(uint16_t opCode, uint8_t numberOfNs)
{
    uint16_t mask = 0xFFFFu >> ((4 - numberOfNs) * 4u);
    return opCode & mask;
}



uint8_t getX(uint16_t opCode)
{
    return (opCode & 0x0F00u) >> 8u;
}



uint8_t getY(uint16_t opCode)
{
    return (opCode & 0x00F0u) >> 4u;
}



void processOpcode(uint16_t opCode)
{
    uint8_t first4Bits = opCode >> 12u;
    uint8_t last4Bits = opCode & 0x000Fu;
    uint8_t lastByte = opCode & 0x00FFu;

    uint8_t* v0 = storage.registers;
    uint8_t* vx = storage.registers + getX(opCode);
    uint8_t* vy = storage.registers + getY(opCode);
    uint8_t* vf = storage.registers + 0xF;

    uint16_t nnn = getNs(opCode, 3);
    uint16_t nn = getNs(opCode, 2);
    uint16_t n = getNs(opCode, 1);

    // Iteradores
    uint8_t* v;
    uint8_t* m;

    uint16_t temp16bit;

    switch (first4Bits)
    {
        case 0x0:
            switch (opCode)
            {
                case 0x00E0:
                    // Clear the display.
                    clearScreen();
                    break;

                case 0x00EE:
                    // Return from a subroutine.
                    //
                    // The interpreter sets the program counter to the address at the top of the stack,
                    // then subtracts 1 from the stack pointer.
                    if(storage.stackPointer > 0)
                        storage.stackPointer--;

                    storage.programCounter = storage.stack[ storage.stackPointer ];
                    break;

                default: // 0NNN
                    // Jump to a machine code routine at nnn.
                    //
                    // This instruction is only used on the old computers on which Chip-8 was originally implemented.
                    // It is ignored by modern interpreters.
                    break;
            }
            break;


        case 0x1: // 1NNN
            // Jump to location nnn.
            //
            // The interpreter sets the program counter to nnn.
            storage.programCounter = nnn - 2; // PC vai ser somado 2 no fim da instrução
            break;


        case 0x2: // 2NNN
            // Call subroutine at nnn.
            //
            // The interpreter increments the stack pointer, then puts the current PC on the top of the stack.
            // The PC is then set to nnn.
            storage.stack[ storage.stackPointer ] = storage.programCounter;
            storage.programCounter = nnn - 2; // PC vai ser somado 2 no fim da instrução

            if(storage.stackPointer < STACK_SIZE - 1)
                storage.stackPointer++;

            break;


        case 0x3: // 3XNN
            // Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
            if(*vx == nn)
                storage.programCounter += 2;
            break;


        case 0x4: // 4XNN
            // Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block)
            if(*vx != nn)
                storage.programCounter += 2;
            break;


        case 0x5: // 5XY0
            // Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)
            if(last4Bits == 0x0 && *vx == *vy)
                storage.programCounter += 2;
            break;


        case 0x6: // 6XNN
            // Sets VX to NN.
            *vx = nn;
            break;


        case 0x7: // 7XNN
            // Adds NN to VX. (Carry flag is not changed)
            *vx += nn;
            break;


        case 0x8:
            switch (last4Bits)
            {
                case 0x0: // 8XY0
                    // Sets VX to the value of VY.
                    *vx = *vy;
                    break;

                case 0x1: // 8XY1
                    // Sets VX to VX or VY. (Bitwise OR operation)
                    *vx |= *vy;
                    break;

                case 0x2: // 8XY2
                    // Sets VX to VX and VY. (Bitwise AND operation)
                    *vx &= *vy;
                    break;

                case 0x3: // 8XY3
                    // Sets VX to VX xor VY.
                    *vx ^= *vy;
                    break;

                case 0x4: // 8XY4
                    // Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
                    temp16bit = *vx + *vy;
                    *vx = temp16bit;
                    if(temp16bit != *vx)
                        *vf = 1;
                    else
                        *vf = 0;
                    break;

                case 0x5: // 8XY5
                    // VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                    if(*vx < *vy)
                        *vf = 0;
                    else
                        *vf = 1;
                    *vx -= *vy;
                    break;

                case 0x6: // 8XY6
                    // Stores the least significant bit of VX in VF and then shifts VX to the right by 1.
                    *vf = *vx % 2u;
                    *vx >>= 1u;
                    break;

                case 0x7: // 8XY7
                    // Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                    if(*vy < *vx)
                        *vf = 0;
                    else
                        *vf = 1;
                    *vx = *vy - *vx;
                    break;

                case 0xE: // 8XYE
                    // Stores the most significant bit of VX in VF and then shifts VX to the left by 1.
                    *vf = *vx >> 7u;
                    *vx <<= 1u;
                    break;
            }
            break;


        case 0x9: // 9XY0
            // Skips the next instruction if VX doesn't equal VY. (Usually the next instruction is a jump to skip a code block)
            if(last4Bits == 0x0 && *vx != *vy)
                storage.programCounter += 2;
            break;


        case 0xA: // ANNN
            // Sets I to the address NNN.
            storage.addressRegister = nnn;
            break;


        case 0xB: // BNNN
            // Jumps to the address NNN plus V0.
            storage.programCounter = nnn + storage.registers[0x0] - 2; // PC vai ser somado 2 no fim da instrução
            break;


        case 0xC: // CXNN
            // Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
            *vx = (uint8_t)(random() % 255) & nn;
            break;


        case 0xD: // DXYN
            // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
            //
            // The interpreter reads n bytes from memory, starting at the address stored in I.
            // These bytes are then displayed as sprites on screen at coordinates (Vx, Vy).
            // Sprites are XORed onto the existing screen. If this causes any pixels to be erased, VF is set to 1,
            // otherwise it is set to 0. If the sprite is positioned so part of it is outside the coordinates of
            // the display, it wraps around to the opposite side of the screen.
            *vf = draw(*vx, *vy, n, &storage.memory[ storage.addressRegister ]);
            break;


        case 0xE:
            switch (lastByte)
            {
                case 0x9E: // EX9E
                    // Skips the next instruction if the key stored in VX is pressed.
                    // (Usually the next instruction is a jump to skip a code block)
                    if( isPressed(*vx) )
                        storage.programCounter += 2;
                    break;

                case 0xA1: // EXA1
                    // Skips the next instruction if the key stored in VX isn't pressed.
                    // (Usually the next instruction is a jump to skip a code block)
                    if( !isPressed(*vx) )
                        storage.programCounter += 2;
                    break;
            }
            break;


        case 0xF:
            switch (lastByte)
            {
                case 0x07: // FX07
                    // Sets VX to the value of the delay timer.
                    *vx = storage.delayTimer;
                    break;

                case 0x0A: // FX0A
                    // A key press is awaited, and then stored in VX.
                    // (Blocking Operation. All instruction halted until next key event)
                    *vx = awaitKey();
                    break;

                case 0x15: // FX15
                    // Sets the delay timer to VX.
                    storage.delayTimer = *vx;
                    break;

                case 0x18: // FX18
                    // Sets the sound timer to VX.
                    storage.soundTimer = *vx;
                    break;

                case 0x1E: // FX1E
                    // Adds VX to I. VF is not affected.
                    storage.addressRegister += *vx;
                    break;

                case 0x29: // FX29
                    // Sets I to the location of the sprite for the character in VX.
                    // Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                    storage.addressRegister = *vx * 0x5;
                    break;

                case 0x33: // FX33
                    // Stores the binary-coded decimal representation of VX, with the most significant of three digits
                    // at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2.
                    // (In other words, take the decimal representation of VX, place the hundreds digit in memory at
                    // location in I, the tens digit at location I+1, and the ones digit at location I+2.)
                    temp16bit = *vx;
                    storage.memory[storage.addressRegister + 2] = temp16bit % 10;

                    temp16bit /= 10;
                    storage.memory[storage.addressRegister + 1] = temp16bit % 10;

                    temp16bit /= 10;
                    storage.memory[storage.addressRegister] = temp16bit % 10;
                    break;

                case 0x55: // FX55
                    // Stores V0 to VX (including VX) in memory starting at address I.
                    // The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                    for(v = v0, m = storage.memory + storage.addressRegister; v <= vx; v++, m++)
                        *m = *v;
                    break;

                case 0x65: // FX65
                    // Fills V0 to VX (including VX) with values from memory starting at address I.
                    // The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                    for(v = v0, m = storage.memory + storage.addressRegister; v <= vx; v++, m++)
                        *v = *m;
                    break;
            }
            break;
    }
}



int processorLoop(void* dummy)
{
    Uint32 lastCountdownTime = SDL_GetTicks();
    Uint32 currentTime;

    clock_t cpuClockOnStart, currentCpuClock;

    uint16_t opCode;
    uint32_t timerDelayMs = 1000 / TIMER_COUNTDOWNS_PER_SECOND;

    while (true)
    {
        currentTime = SDL_GetTicks();
        if(currentTime - lastCountdownTime >= timerDelayMs)
        {
            countdownTimers();
            lastCountdownTime = currentTime;
        }

        opCode = (storage.memory[storage.programCounter] << sizeof(uint8_t) * 8) |
                 storage.memory[storage.programCounter + 1];

        cpuClockOnStart = clock();
        processOpcode(opCode);
        storage.programCounter += 2;

        while (clock() - cpuClockOnStart < REAL_CLOCK_CYCLES_PER_INSTRUCTION);
    }
}