#ifndef CHIP8_CONTROLLERS_H
#define CHIP8_CONTROLLERS_H

#include <stdbool.h>
#include <stdint.h>

#define NUMBER_OF_KEYS 0x10
#define BINDINGS_PER_KEY 2

uint8_t g_controllerConfig[NUMBER_OF_KEYS][BINDINGS_PER_KEY];

bool isPressed(uint8_t key);
uint8_t awaitKey();

#endif //CHIP8_CONTROLLERS_H
