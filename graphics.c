#include "graphics.h"

#include <SDL.h>
#include <stdbool.h>



SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

// Provisório, transferir para memória em storage.h em bits no futuro
bool screen[HEIGHT][WIDTH];



void graphicsStart()
{
    SDL_Init(SDL_INIT_EVERYTHING);

    window = SDL_CreateWindow("CHIP-8", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              WIDTH * PIXEL_MULTIPLIER, HEIGHT * PIXEL_MULTIPLIER,
                              SDL_WINDOW_SHOWN);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    clearScreen();
}



void graphicsClose()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}



void refresh()
{
    if(window == NULL || renderer == NULL)
        return;

    uint8_t h, w;
    SDL_Rect pixel;
    pixel.w = pixel.h = PIXEL_MULTIPLIER;

    for (h = 0; h < HEIGHT; h++)
    {
        for (w = 0; w < WIDTH; w++)
        {
            if(screen[h][w])
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, SDL_ALPHA_OPAQUE);
            else
                SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, SDL_ALPHA_OPAQUE);

            pixel.x = w * PIXEL_MULTIPLIER;
            pixel.y = h * PIXEL_MULTIPLIER;
            SDL_RenderFillRect(renderer, &pixel);
        }
    }

    SDL_RenderPresent(renderer);
}



int graphicsLoop(void* dummy)
{
    if(window == NULL || renderer == NULL)
        return 0;

    bool running = true;
    SDL_Event event;

    uint32_t frameDelayMs = 1000 / FRAMES_PER_SECOND;

    while (running)
    {
        refresh();

        SDL_Delay(frameDelayMs);

        while ( SDL_PollEvent(&event) )
        {
            if(event.type == SDL_QUIT)
            {
                running = false;
                break;
            }
        }
    }

    return 0;
}



void clearScreen()
{
    uint8_t h, w;

    for (h = 0; h < HEIGHT; h++)
    {
        for (w = 0; w < WIDTH; w++)
        {
            screen[h][w] = false;
        }
    }
}



uint8_t draw(uint8_t baseX, uint8_t baseY, uint8_t height, const uint8_t* buffer)
{
    uint8_t returnValue = 0;

    uint8_t yOffset, xOffset, x, y, mask;
    bool pixelValue;
    for(yOffset = 0; yOffset < height; yOffset++)
    {
        mask = 0b10000000;
        for(xOffset = 0; xOffset < 8; xOffset++)
        {
            pixelValue = (buffer[yOffset] & mask) >> (7 - xOffset);

            x = (baseX + xOffset) % WIDTH;
            y = (baseY + yOffset) % HEIGHT;

            bool previous = screen[y][x];
            screen[y][x] = (!screen[y][x] && pixelValue) || (screen[y][x] && !pixelValue);

            if(previous && !screen[y][x])
                returnValue = 1;

            mask >>= 1u;
        }
    }

    return returnValue;
}